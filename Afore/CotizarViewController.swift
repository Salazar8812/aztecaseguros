//
//  CotizarViewController.swift
//  Afore
//
//  Created by antonio lavin on 10/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CotizarViewController: UIViewController {
    @IBOutlet weak var iconRightImageView: UIImageView!
    var dataIconRight : String?
    var dataColor: Int?
    var dataTitleBar: String?
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var backIconImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(dataIconRight != ""){
            addIconRight()
        }

        if(dataTitleBar != ""){
            titleBarLabel.text = dataTitleBar
        }
        
        titleBarLabel.textColor = UIColor (netHex:dataColor!)
        
        backButton.setTitleColor(UIColor (netHex:dataColor!), for: .normal)
        
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:dataColor!)
        
        // Do any additional setup after loading the view.
    }
    
    func addIconRight(){
        iconRightImageView.image = UIImage(named:dataIconRight!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func newCotizacionButton(_ sender: Any) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "NewCotizacionViewController") as! NewCotizacionViewController
        navigationController?.pushViewController(myVC, animated: true)
    }

    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
