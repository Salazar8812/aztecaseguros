//
//  CellItem.swift
//  Afore
//
//  Created by Charls Salazar on 06/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CellItem: UITableViewCell {

    @IBOutlet weak var vinetaImageView: UIImageView!
    @IBOutlet weak var itemInfoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
