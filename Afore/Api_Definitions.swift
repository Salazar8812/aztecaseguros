//
//  Definitions.swift
//  Afore
//
//  Created by Charls Salazar on 05/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class Api_Definitions: NSObject{
    
    static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Authorization" : "Basic b2FndXNlcjpvNGd1czNy",
        "Accept" : "application/json"
    ]
    
    /// API
    
    static let API = "http://totalvideos.dflabs.io"
    static let API_TOTAL = "https://msstest.totalplay.com.mx"
    static let API_TOTAL_TEST = "https://mss.totalplay.com.mx"

    
    ////// SETTINGS PACKAGE
    static let API_QUERY_TYPE_PLAIN = API_TOTAL + "/ventasmovil/TipoPlanJson"
    

}
