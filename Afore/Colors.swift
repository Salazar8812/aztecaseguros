//
//  Colors.swift
//  Afore
//
//  Created by Charls Salazar on 07/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
}

class Colors{
    static let colorAbout: Int = 0x008335
    static let colorVidaMax: Int = 0x008335
    static let colorWoman: Int = 0xEF359D
    static let colorHeartAttack: Int = 0xE43733
    static let colorAccident: Int = 0x5FB404
    static let colorEducation: Int = 0x04B4AE
    static let colorTranquility: Int = 0x734297
    static let colorMigrate: Int = 0xFFC300
    static let colorItalika: Int = 0x0062BF
    static let colorCar: Int = 0xF55E2D
    static let colorMoto: Int = 0x59A9E2
    static let colorSinester: Int = 0xA3B14B
}

class ColorsRGB{
    static let colorAbout: String = "008335"
    static let colorVidaMax: String = "008335"
    static let colorWoman: String = "EF359D"
    static let colorHeartAttack: String = "E43733"
    static let colorAccident: String = "5FB404"
    static let colorEducation: String = "04B4AE"
    static let colorTranquility: String = "734297"
    static let colorMigrate: String = "FFC300"
    static let colorItalika: String = "0062BF"
    static let colorCar: String = "F55E2D"
    static let colorMoto: String = "59A9E2"
    static let colorSinester: String = "A3B14B"
}
