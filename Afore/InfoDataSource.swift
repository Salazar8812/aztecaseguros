//
//  InfoDataSource.swift
//  Afore
//
//  Created by Charls Salazar on 05/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit

class InfoDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var infoArray : [String] = []
    var color : Int?
    init(tableView:UITableView,color:Int) {
        super.init()
        self.color = color
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
    }
    
    
    func update(coleccionInfo: [String]){
        self.infoArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CellItem"
        let cell:CellItem! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! CellItem
        let info = self.infoArray[indexPath.row]
        
        let attrStr = try! NSAttributedString(
            data: info.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil)
        
        cell.itemInfoLabel.attributedText = attrStr
        //cell.itemInfoLabel.textColor = UIColor(netHex:color!)
        cell.vinetaImageView.image = cell.vinetaImageView.image!.withRenderingMode(.alwaysTemplate)
        cell.vinetaImageView.tintColor = UIColor(netHex:color!)
        return cell
    }
    
}
