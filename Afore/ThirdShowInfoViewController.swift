//
//  ThirdShowInfoViewController.swift
//  Afore
//
//  Created by Charls Salazar on 07/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class ThirdShowInfoViewController: UIViewController {

    var infoDataSource : InfoDataSource?
    var vc: ViewControllerUtils?
    var dataArray: [String]=[]
    var dataShowIconRight: String?
    var dataTitleL:String?
    var dataTitleB: String?
    var dataTitleBarL:String?
    var dataPlan1 : String?
    var dataPlan2 : String?
    var color:Int?
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBarLabel: UILabel!

    @IBOutlet weak var iconRightImageView: UIImageView!
    @IBOutlet weak var plansButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var backIconImageView: UIImageView!
    
    override func viewDidLoad() {
        let nib = UINib(nibName: "CellItem", bundle: nil)
        infoTableView.register(nib, forCellReuseIdentifier: "CellItem")
        infoTableView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        self.infoTableView.estimatedRowHeight = 500
        self.infoTableView.rowHeight = UITableViewAutomaticDimension
        
        self.infoTableView.setNeedsLayout()
        self.infoTableView.layoutIfNeeded()
        
        titleLabel.text = dataTitleL;
        titleLabel.textColor = UIColor(netHex:color!)
        plansButton.setTitle(dataTitleB, for: UIControlState.normal)
        
        infoDataSource = InfoDataSource(tableView: self.infoTableView, color:color!)
        infoDataSource?.update(coleccionInfo: dataArray)
        if(dataShowIconRight != ""){
            addRightImage(name: dataShowIconRight!)
        }
        
        if(dataTitleBarL != ""){
            titleBarLabel.text = dataTitleBarL!;
            titleBarLabel.textColor = UIColor(netHex:color!)
        }
        
        backButton.setTitleColor(UIColor (netHex:color!) , for: .normal)
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:color!)
    }
    
    @IBAction func cotizarButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "NewCotizacionViewController") as!
            NewCotizacionViewController
        
        present(myVC, animated: true, completion: nil)
    }
    func addRightImage(name:String){
        iconRightImageView.image = UIImage(named: dataShowIconRight!)
    }
    @IBAction func coverageButton(_ sender: Any) {
      
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PlansViewController") as! PlansViewController
        myVC.dataPlanOneImg = dataPlan1
        myVC.dataPlanTwoImg = dataPlan2
        myVC.dataShowImgRight = dataShowIconRight
        myVC.dataTitleBarL = "Protección Italika"
        myVC.dataColor = color!
        present(myVC, animated: true, completion: nil)
    
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }


}
