//
//  NewCotizacionViewController.swift
//  Afore
//
//  Created by antonio lavin on 10/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class NewCotizacionViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var pickerViewTextField : UITextField?
    var yourItems=[String]()

    override func viewDidLoad() {
        //super.viewDidLoad()
        pickerViewTextField = UITextField(frame: CGRect.zero)
        view.addSubview(pickerViewTextField!)
        let pickerView = UIPickerView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
        pickerView.backgroundColor = .white
        
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        // set change the inputView (default is keyboard) to UIPickerView
        pickerViewTextField?.inputView = pickerView
        // add a toolbar with Cancel & Done button
        let toolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("DonePicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("donetoPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerViewTextField?.inputView = pickerView
        pickerViewTextField?.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    @IBAction func modelButton(_ sender: Any) {
        yourItems=["2017", "2016", "2015","2014", "2015", "2010"]
        pickerViewTextField?.becomeFirstResponder()

    }
   
    @IBAction func brandButton(_ sender: Any) {
        yourItems=["Italika"]
        pickerViewTextField?.becomeFirstResponder()
    }
    @IBAction func typeButton(_ sender: Any) {
        yourItems=["CHOPPER","DEPORTIVA","DOBLE PROPOSITO","ELECTRICA","RECREATIVAS","SCOOTER/MOTONETA","TRABAJO"]
        pickerViewTextField?.becomeFirstResponder()
    }
    @IBAction func descriptionButton(_ sender: Any) {
        yourItems=["RC150ROJA","RC150GT NEGRA","TC200","TC200 BLANCA","TC250"]
        pickerViewTextField?.becomeFirstResponder()
    }
    @IBAction func typeUsedButton(_ sender: Any) {
        yourItems=["PARTICULAR","COMERCIAL"]
        pickerViewTextField?.becomeFirstResponder()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func cancelTouched(_ sender: UIBarButtonItem) {
        // hide the picker view
        pickerViewTextField?.resignFirstResponder()
    }
    
    func doneTouched(_ sender: UIBarButtonItem) {
        // hide the picker view
        pickerViewTextField?.resignFirstResponder()
        // perform some action
    }
    
    func donePicker() {
        pickerViewTextField?.resignFirstResponder()
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yourItems.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item: String? = (yourItems[row] as String)
        return item!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item: String? = (yourItems[row] as String)
        NSLog(item!)
    }
    
    /*@IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }*/
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    

}
