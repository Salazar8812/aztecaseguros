//
//  ShowInfoViewController.swift
//  Afore
//
//  Created by Charls Salazar on 05/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import Foundation

class ShowInfoViewContoller: UIViewController {
    
    @IBOutlet weak var infoTableView: UITableView!

    var infoDataSource : InfoDataSource?
    var vc: ViewControllerUtils?
    var dataArray: [String]=[]
    var dataShowIconRight: String?
    var dataTitleL : String?
    var dataTitleBarL: String?
    var color: Int?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var iconRightImageView: UIImageView!
    
    @IBOutlet weak var backIconImageView: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    
    
    override func viewDidLoad() {
        let nib = UINib(nibName: "CellItem", bundle: nil)
        infoTableView.register(nib, forCellReuseIdentifier: "CellItem")
        infoTableView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        self.infoTableView.estimatedRowHeight = 150
        self.infoTableView.rowHeight = UITableViewAutomaticDimension
    
        self.infoTableView.setNeedsLayout()
        self.infoTableView.layoutIfNeeded()
        
        infoDataSource = InfoDataSource(tableView: self.infoTableView, color:color!)
        infoDataSource?.update(coleccionInfo: dataArray)
        
        if(dataShowIconRight != ""){
          addRightImage(name: dataShowIconRight!)
        }
        
        if(dataTitleL != ""){
            //titleLabel.text = dataTitleL!;
            //titleLabel.textColor = UIColor(netHex:color!)
        }
        
        if(dataTitleBarL != ""){
            titleBarLabel.text = dataTitleBarL!;
            titleBarLabel.textColor = UIColor(netHex:color!)
        }
        
        backButton.setTitleColor(UIColor (netHex:color!) , for: .normal)
        
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:color!)
    }
   
    func addRightImage(name:String){
        iconRightImageView.image = UIImage(named: dataShowIconRight!)
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
}
