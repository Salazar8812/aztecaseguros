//
//  AlertDialog.swift
//  senasica
//
//  Created by Lennken Group on 03/09/16.
//  Copyright © 2016 Lennken Group. All rights reserved.
//

import UIKit
import PKHUD

class AlertDialog {
    
    static var overlay : UIView?
    static var viewController : UIViewController?

    static func show
        
        (title: String, body: String, view : UIViewController ){
        let refreshAlert = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        view.present(refreshAlert, animated: true, completion: nil)
    }
    
    @objc static func pressed(){
    
    }
    
    static func showOverlay(){
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    static func hideOverlay(){
        PKHUD.sharedHUD.hide(true)
    }
    
}
