//
//  SplashScreenViewController.swift
//  Afore
//
//  Created by Charls Salazar on 03/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit
import SwiftyGif

class SplashScreenViewController: UIViewController {
    @IBOutlet weak var imgSplashImageView: UIImageView!
    
    override func viewDidLoad() {
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "introduccion")
        //imgSplashImageView = UIImageView(gifImage: gif, manager: gifmanager)
        //imgSplashImageView.frame = CGRect(x: 0.0, y: 5.0, width: 400.0, height: 200.0)
        //view.addSubview(imgSplashImageView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as UIViewController
            self.present(vc, animated: true, completion: nil)
        })
        
    }
}
