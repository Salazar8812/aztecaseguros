//
//  SinesterLifeResponse.swift
//  Afore
//
//  Created by Charls Salazar on 05/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class SinesterLifeResponse : NSObject,Mappable{
    
    var errorCode : String?
    var errorDesc : String?
    var historico : String?
    var comentario : String?
    var fecha : String?
    var fechaComen : String?
    var producto : String?
    var status : String?
    var sucursal : String?
   
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        errorCode		<- map["errorCode"]
        errorDesc		<- map["errorDesc"]
        historico		<- map["historico"]
        comentario		<- map["comentario"]
        fecha		<- map["fecha"]
        fechaComen		<- map["fechaComen"]
        producto		<- map["producto"]
        status		<- map["status"]
        sucursal		<- map["sucursal"]
    }

}
