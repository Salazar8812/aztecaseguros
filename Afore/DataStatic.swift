//
//  DataStatic.swift
//  Afore
//
//  Created by Charls Salazar on 26/04/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation

class DataStatic{
    //Quienes Somos
    static let aboutArray : [String] = [
    "<p style=\"font-family:verdana;\">Seguros Azteca es una empresa mexicana que protege el bienestar y el patrimonio de nuestros clientes y sus familias con productos y servicios de excelencia.</p>",
    
        "<p style=\"font-family:verdana;\">Atiende a un segmento de la población ignoradopor la industria aseguradora.</p>",
        
        "<p style=\"font-family:verdana;\">Ofrece productos sencillos, fáciles de adquirir y con precios muy accesibles.</p>",
        
        "<p style=\"font-family:verdana;\">Atendemos los 365 días del año de 9 a 9.</p>"]
    
    static let vidaMaxArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico para tu familia en <br>caso de que llegues a fallecer por causa<br>natural o accidental: <br>&nbsp;-Hasta $60,000 por muerte natural. <br>&nbsp;-Hasta $120,000 por muerte accidental.</p>",
    
    "<p style=\"font-family:verdana;\">Incluye la liquidación de la deuda del crédito.</p>",
    
    "<p style=\"font-family:verdana;\">Su vigencia es la misma del crédito contratado.</p>",
    
    "<p style=\"font-family:verdana;\">Requisitos: <br> &nbsp; &nbsp; -Tener entre 18 y 75 años. <br> &nbsp; &nbsp; -Solicitarlo junto con un credito de Banco Azteca.</p>"
    ]
    
    static let womanArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de $50,000 en caso de primer diagnóstico de cáncer de mama\no cervicouterino.</p>",
    "<p style=\"font-family:verdana;\">Incluye: <br>&nbsp;&nbsp;-20 consultas psicológicas a domicilio. <br>&nbsp;&nbsp;-Consultas Psicológicas por teléfono sin limite.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activación, con un periodo al descubierto de 90 días</p>",
    "<p style=\"font-family:verdana;\">Requisitos: <br>&nbsp;&nbsp;-Tener entre 15 y 64 años. <br>&nbsp;&nbsp;-No haber sido diagnosticada con ningún tipo de cancer.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra por sólo <b>$250</b> al año.</p>"
    ]
    
    static let heartAttackArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de $50,000 en caso<br>de primer ocurrencia de infarto al miocardio.</p>",
    "<p style=\"font-family:verdana;\">Incluye: <br>&nbsp;&nbsp;-Envío de ambulancia (una vez al año). <br>&nbsp;&nbsp;-Consultas médicas telefónicas sin límite.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activación, con un periodo al descubierto de 30 días.</p>",
    "<p style=\"font-family:verdana;\">Requisitos: <br>&nbsp;&nbsp;-Tener entre 15 y 64 años. <br>&nbsp; -No haber tenido ningún padecimiento del corazón.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra por sólo <b>$250</b> al año.</p>"
    ]
    
    static let accidentArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de $50,000 en caso de fallecimiento por accidente.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activación.</p>",
    "<p style=\"font-family:verdana;\">Requisito: <br>&nbsp;&nbsp;-Tener entre 18 y 70 años.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra por sólo <strong>$150</strong> al año.</p>"
    ]
    
    static let educationArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de 6 mensualidades de hasta $4,000 para garantizar los estudios de los hijos, en caso de que el asegurado sufra: <br>&nbsp;&nbsp;-Fallecimiento.<br>&nbsp;&nbsp;-Invalidez total y permanente. <br>&nbsp;&nbsp;-Diagnóstico de una enfermedad grave: <br> &nbsp&nbsp;&nbsp;&nbsp; >Cáncer. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Infarto al miocardio. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Apoplejía. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Cirugía coronaria. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Esclerosis múltiple. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Insuficiencia renal crónica. <br> &nbsp;&nbsp;&nbsp;&nbsp; >Transplante de óraganos vitales.</p>",
    "<p style=\"font-family:verdana;\">Incluye maestro por télefono para: <br> &nbsp;&nbsp;-Solución de tareas(primaria y secundaria). <br> &nbsp;&nbsp;-Guía de tareas de acuerdo con la SEP. <br> &nbsp;&nbsp;-Referencias bibliográficas y sitios de internet. <br>&nbsp;&nbsp;-Información escolar, métodos, etcétera.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activación.</p>",
    "<p style=\"font-family:verdana;\">Requisito: <br>&nbsp;&nbsp;-Tener entre 16 y 75 años.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra por sólo <b>$350</b> al año.</p>"
    ]
    
    static let tranquilityArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de $50,000 por fallecimiento individual y/o familiar de los asegurados por causa natural o accidental.</p>",
    "<p style=\"font-family:verdana;\">Incluye servicio funerario.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activación,con un periodo al descubierto de 45 días por fallecimiento natural.</p>",
    "<p style=\"font-family:verdana;\">Requisito: <br>&nbsp;&nbsp;-Tener entre 18 y 65 años (hijos de hasta 23 años).</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra desde <b>$500</b> al año.</p>"
    ]
    
    static let migrateArray : [String] = [
    "<p style=\"font-family:verdana;\">Respaldo económico de hasta $50,000 en caso de fallecimiento por causa natural o accidental del migrante y/o familiar asegurado.</p>",
    "<p style=\"font-family:verdana;\">Incluye:<br>-Regreso del migrante en caso de fallecimiento, al lugar de residencia del asegurado.<br>-Servicio funerario para el migrante.<br>-Paquete de descuento y servicios para el asegurado dentro de la República Mexicana.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de la activacón.</p>",
    "<p style=\"font-family:verdana;\">Requisitos:<br>-Tener entre 18 y 70 años.<br>-El asegurado debe vivir dentro de la República Mexicana.<br>-El migrante debe ser hijo(a) o cónyuge del asegurado y residir en EUA, Canadá (excepto Alaska, Puerto Rico y Hawái) o algún otro estado de la Republica Mexicana diferente al del asegurado.</p>",
    "<p style=\"font-family:verdana;\">Diponible en cuaquier sucursal de Banco Azteca o Elektra desde <b>$350</b> al año.</p>"
    ]
    
    static let italikaArray : [String] = [
    "<p style=\"font-family:verdana;\">Protección para tu Italika en caso de pérdida total por robo o accidente y por ocasionar daños a terceros.</p>",
    "<p style=\"font-family:verdana;\">Incluye: <br>&nbsp;&nbsp;-Apoyo vial. <br>&nbsp;&nbsp;-Gastos legales y de fianza.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual, con un periodo al descubierto de 60 días por robo total, en caso de adquirir la protección después de 8 días de haber comprado la Italika.</p>",
    "<p style=\"font-family:verdana;\">Requisitos: <br> &nbsp; &nbsp;-Es únicamente para Italikas. <br> &nbsp; &nbsp;-Presentar factura original e identificación oficial.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra.</p>"
    ]
    
    static let carArray : [String] = [
    "<p style=\"font-family:verdana;\">Protección y respaldo para ti en caso de robo o accidente de tu auto.</p>",
    "<p style=\"font-family:verdana;\">Según el plan contratado incluye:<br>-Daños materiales.<br>-Robo total.<br>-Asistencia vial.<br>-Gastos médicos.<br>-Gastos legales.<br>-Daños a terceros.</p>",
    "<p style=\"font-family:verdana;\">Requisitos:<br>-Identificación oficial y tarjeta de circulación.<br>-Código postal de tu domicilio.<br>-Tener un vehículo de uso particular, no mayor a 30 años de antigüedad o una pick up de hasta 2 ½ toneladas con máximo 21 años de antigüedad.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra.</p>"
    ]
    
    static let motoArray : [String] = [
    "<p style=\"font-family:verdana;\">Protección en caso de que ocasiones daños en bienes materiales o personas por manejar tu moto.</p>",
    "<p style=\"font-family:verdana;\">Incluye: <br>&nbsp;&nbsp;-Honorarios de abogados. <br>&nbsp;&nbsp;-Gastos del proceso legal.</p>",
    "<p style=\"font-family:verdana;\">Su vigencia es anual a partir de su activación.</p>",
    "<p style=\"font-family:verdana;\">Requisitos:<br>&nbsp;&nbsp;-Identificación oficial. <br> &nbsp;-Factura Original o Carta Factura de la moto.</p>",
    "<p style=\"font-family:verdana;\">Disponible en cualquier sucursal de Banco Azteca o Elektra.</p>"
    ]

}
