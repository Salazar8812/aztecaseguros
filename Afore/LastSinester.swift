//
//  LastSinester.swift
//  Afore
//
//  Created by Charls Salazar on 05/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class LastSinester: NSObject{
    var comentario : String?
    var fecha : String?
    var fechaComen : String?
    var producto : String?
    var status : String?
    var sucursal : String?
    var ultimoSiniestro: LastSinester?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        comentario		<- map["comentario"]
        fecha		<- map["fecha"]
        fechaComen		<- map["fechaComen"]
        producto		<- map["producto"]
        status		<- map["status"]
        sucursal		<- map["sucursal"]
        ultimoSiniestro		<- map["ultimoSiniestro"]
    }
    
}
