//
//  SecondShowInfoViewController.swift
//  Afore
//
//  Created by Charls Salazar on 06/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class SecondShowInfoViewController: UIViewController {

    @IBOutlet weak var infoTableView: UITableView!    
    var infoDataSource : InfoDataSource?
    var vc: ViewControllerUtils?
    var dataArray: [String]=[]
    var dataShowIconRight: String?
    var dataTitleBarL: String?
    var dataTitleL:String?
    var dataTitleB: String?
    var dataPlan1:String?
    var dataPlan2:String?
    var color:Int?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var plansButton: UIButton!
    
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var iconRightImageButton: UIImageView!
    
    @IBOutlet weak var backIconImageView: UIImageView!
    
    
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        let nib = UINib(nibName: "CellItem", bundle: nil)
        infoTableView.register(nib, forCellReuseIdentifier: "CellItem")
        infoTableView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        self.infoTableView.estimatedRowHeight = 500
        self.infoTableView.rowHeight = UITableViewAutomaticDimension
        
        self.infoTableView.setNeedsLayout()
        self.infoTableView.layoutIfNeeded()
    
        titleLabel.text = dataTitleL;
        titleLabel.textColor = UIColor(netHex:color!)
        plansButton.setTitle(dataTitleB, for: UIControlState.normal)
            
        infoDataSource = InfoDataSource(tableView: self.infoTableView, color:color!)
        infoDataSource?.update(coleccionInfo: dataArray)
        if(dataShowIconRight != ""){
            addRightImage(name: dataShowIconRight!)
        }
        
        if(dataTitleBarL != ""){
            titleBarLabel.text = dataTitleBarL!;
            titleBarLabel.textColor = UIColor(netHex:color!)
        }
        
        backButton.setTitleColor(UIColor (netHex:color!) , for: .normal)
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:color!)
    }
    
    func addRightImage(name:String){
        iconRightImageButton.image = UIImage(named: dataShowIconRight!)
    }
    
    @IBAction func plansButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PlansViewController") as! PlansViewController
        myVC.dataPlanOneImg = dataPlan1
        myVC.dataPlanTwoImg = dataPlan2
        myVC.dataShowImgRight = dataShowIconRight
        myVC.dataTitleBarL = dataTitleBarL
        myVC.dataColor = color!
        present(myVC, animated: true, completion: nil)
    }
   
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}
