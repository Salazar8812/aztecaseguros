//
//  ViewController.swift
//  Afore
//
//  Created by Charls Salazar on 26/04/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import PDFReader

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chatButton(sender: AnyObject) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "chatViewController") as! chatViewController
        navigationController?.present(myVC, animated: true)
    }

    @IBAction func noticePrivacyButton(_ sender: UIButton) {
        let asset = NSDataAsset(name: "dt_noticePrivacy")
        let data = asset?.data
        let document = PDFDocument(fileData: data!, fileName: "Aviso de Privacidad")!
        let readerController = PDFViewController.createNew(with: document)
        navigationController?.present(readerController, animated: true)
    }

    @IBAction func aboutButton(_ sender: UIButton) {
                
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ShowInfoViewContoller") as! ShowInfoViewContoller
        myVC.dataArray = DataStatic.aboutArray
        myVC.dataShowIconRight = "ic_qs"
        myVC.dataTitleL = ""
        myVC.dataTitleBarL="¿Quiénes Somos?"
        myVC.color = Colors.colorAbout
        present(myVC, animated: true, completion: nil)

    }
    
    @IBAction func vidamaxButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.vidaMaxArray
        myVC.dataShowIconRight = "ic_vidamax"
        myVC.dataTitleL = "Cuidamos tu familia\nsi llegaras a faltar"
        myVC.dataTitleB = "Planes"
        myVC.dataPlan1 = "t_planes_vidamax";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nVidamax"
        
        myVC.color = Colors.colorVidaMax
        present(myVC, animated: true, completion: nil)

    }
    
    @IBAction func womanButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ShowInfoViewContoller") as! ShowInfoViewContoller
        myVC.dataArray = DataStatic.womanArray
        myVC.dataShowIconRight = "ic_mujer"
        myVC.dataTitleL = "Te damos un apoyo para tu \n recuperación"
        myVC.dataTitleBarL="Protección\nMujer"
        
        myVC.color = Colors.colorWoman
        present(myVC, animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func hearAttackButton(_ sender: UIButton) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ShowInfoViewContoller") as! ShowInfoViewContoller
        myVC.dataArray = DataStatic.heartAttackArray
        myVC.dataShowIconRight = "ic_infarto"
        myVC.dataTitleL = "Cuidamos tu corazón para \n que siga adelante"
        myVC.dataTitleBarL="Protección\nInfarto"
        
        myVC.color = Colors.colorHeartAttack

        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func accidentButton(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ShowInfoViewContoller") as! ShowInfoViewContoller
        myVC.dataArray = DataStatic.accidentArray
        myVC.dataShowIconRight = "ic_accidente"
        myVC.dataTitleL = "Respaldamos a tu familia \n en caso de imprevisto"
        myVC.dataTitleBarL="Protección\nAccidente"
        
        myVC.color = Colors.colorAccident

        
        present(myVC, animated: true, completion: nil)

    }
   
    @IBAction func educationButton(_ sender: Any) {
      
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.educationArray
        myVC.dataShowIconRight = "ic_educacion"
        myVC.dataTitleL = "Esta es la mejor herencia \n que les puedes dar"
        myVC.dataTitleB = "Planes"
        myVC.dataPlan1 = "t_planes_educacion";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nEducación"
        myVC.color = Colors.colorEducation

        present(myVC, animated: true, completion: nil)
    }
  
    @IBAction func tranquilityButton(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.tranquilityArray
        myVC.dataShowIconRight = "ic_tranquilidad"
        myVC.dataTitleL = "Nos encargamos de todo para \n que tu familia no se preocupe"
        myVC.dataTitleB = "Planes"
        myVC.dataPlan1 = "t_planes_tranquilidad";
        myVC.dataPlan2 = "t_planes_tranquilidad_2";
        myVC.dataTitleBarL="Protección\nTranquilidad"
        
        myVC.color = Colors.colorTranquility
        
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func migrateButton(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.migrateArray
        myVC.dataShowIconRight = "ic_migrante"
        myVC.dataTitleL = "Aseguramos su regreso a tu lado"
        myVC.dataTitleB = "Planes"
        myVC.dataPlan1 = "t_planes_migrante";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nMigrante"
        
        myVC.color = Colors.colorMigrate
        
        present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func italikButton(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ThirdShowInfoViewController") as! ThirdShowInfoViewController
        myVC.dataArray = DataStatic.italikaArray
        myVC.dataShowIconRight = "ic_italika"
        myVC.dataTitleL = "Aseguramos tu Italika ante los imprevistos"
        myVC.dataTitleB = "Coberturas"
        myVC.dataPlan1 = "t_coberturas_italika";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nItalika"
        
        myVC.color = Colors.colorItalika
        
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func carButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.carArray
        myVC.dataShowIconRight = "ic_auto"
        myVC.dataTitleL = "Para que protejas tu \n auto de cualquier imprevisto"
        myVC.dataTitleB = "Coberturas"
        myVC.dataPlan1 = "t_coberturas_auto";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nAuto"
        
        myVC.color = Colors.colorCar
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func motoButton(_ sender: Any) {
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SecondShowInfoViewController") as! SecondShowInfoViewController
        myVC.dataArray = DataStatic.motoArray
        myVC.dataShowIconRight = "ic_moto_rc"
        myVC.dataTitleL = "Te apoyamos para cubrir los daños que ocasiones con tu moto"
        myVC.dataTitleB = "Coberturas"
        myVC.dataPlan1 = "t_planes_moto_rc";
        myVC.dataPlan2 = "";
        myVC.dataTitleBarL="Protección\nMoto RC"
        
        myVC.color = Colors.colorMoto
        present(myVC, animated: true, completion: nil)
    }
    

    @IBAction func querySinesterButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SinesterViewController") as! SinesterViewController
        myVC.dataIconRight="ic_consul_siniestro"
        myVC.dataColor = Colors.colorSinester
        present(myVC, animated: true, completion: nil)
    }
    
    
    
}

