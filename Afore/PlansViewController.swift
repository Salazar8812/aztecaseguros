//
//  PlansViewController.swift
//  Afore
//
//  Created by Charls Salazar on 07/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ImageScrollView

class PlansViewController: UIViewController {
    var dataPlanOneImg : String?
    var dataPlanTwoImg : String?
    var dataShowImgRight : String?
    var dataTitleBarL: String?
    var dataColor: Int?

    @IBOutlet weak var iconRightImageView: UIImageView!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var backIconImageView: UIImageView!
    
    @IBOutlet weak var planOneImageScrollView: ImageScrollView!
    @IBOutlet weak var planTwoImageScrollView: ImageScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    
        if(dataPlanOneImg != ""){
            let myImage = UIImage(named: dataPlanOneImg!)
            planOneImageScrollView.display(image: myImage!)
        }else{
            ViewUtils.setVisibility(view: planOneImageScrollView, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: planOneImageScrollView, visibility: .GONE)
        }
        
        if(dataPlanTwoImg != ""){
            let myImage = UIImage(named: dataPlanTwoImg!)
            planTwoImageScrollView.display(image: myImage!)
        }else{
            ViewUtils.setVisibility(view: planTwoImageScrollView, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: planTwoImageScrollView, visibility: .GONE)
        }
        
        if(dataTitleBarL != ""){
            titleBarLabel.text = dataTitleBarL
            titleBarLabel.textColor = UIColor (netHex:dataColor!)
        }
        
        addRightImage(name: dataShowImgRight!)
        
        backButton.setTitleColor(UIColor (netHex:dataColor!) , for: .normal)
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:dataColor!)
    }

    func addRightImage(name:String){
        iconRightImageView.image = UIImage(named: name)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*@IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }*/
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

}
