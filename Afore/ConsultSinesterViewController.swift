//
//  ConsultSinesterViewController.swift
//  Afore
//
//  Created by antonio lavin on 22/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class ConsultSinesterViewController: UIViewController {
    @IBOutlet weak var backIconImageView: UIImageView!

    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var rightImageView: UIImageView!
    
    var dataColor : Int?
    var dataIconRight : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.setTitleColor(UIColor (netHex:dataColor!) , for: .normal)
        addRightIcon()
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:dataColor!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addRightIcon(){
        rightImageView.image = UIImage(named: dataIconRight!)
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
