//
//  DamageSinesterRequest.swift
//  Afore
//
//  Created by Charls Salazar on 05/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class DamageSinesterRequest: NSObject,Mappable{
    var foliosin : String?
    var fecnaci : String?
    
    override init() {
        super.init()
    }
    
    init(foliosin: String, fecnaci: String) {
        self.foliosin = foliosin
        self.fecnaci=fecnaci
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        foliosin <- map["fecnaci"]
        fecnaci <- map["fecnaci"]
    }
}

