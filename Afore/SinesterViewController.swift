//
//  SinesterViewController.swift
//  Afore
//
//  Created by antonio lavin on 17/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class SinesterViewController: UIViewController {

    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var rightIconImageButton: UIImageView!
   
    var dataIconRight : String?
    var dataColor: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        backButton.setTitleColor(UIColor (netHex:dataColor!) , for: .normal)
        addRightIcon()
        backImageView.image = backImageView.image!.withRenderingMode(.alwaysTemplate)
        backImageView.tintColor = UIColor (netHex:dataColor!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func consultLifeButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ConsultClaimsViewController") as! ConsultClaimsViewController
        myVC.dataColor = dataColor
        myVC.dataIconRight = dataIconRight
        present(myVC, animated: true, completion: nil)
    }

    
        
    @IBAction func damageButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ConsultSinesterViewController") as! ConsultSinesterViewController
        myVC.dataColor = dataColor
        myVC.dataIconRight = dataIconRight
        present(myVC, animated: true, completion: nil)
    }
    
    
    
    func addRightIcon(){
        rightIconImageButton.image = UIImage(named: dataIconRight!)
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
   
                
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
