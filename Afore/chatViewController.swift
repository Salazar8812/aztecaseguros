//
//  chatViewController.swift
//  Afore
//
//  Created by Charls Salazar on 03/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit

class chatViewController: UIViewController {
    @IBOutlet weak var chatWebView: UIWebView!
    
    
    override func viewDidLoad() {
        AlertDialog.showOverlay()
        chatWebView.loadRequest(URLRequest(url: URL(string: "https://tkm.s1gateway.com/integrations/chats/chat_seguros_azteca/index.html?cpgid=1442208")!))
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            AlertDialog.hideOverlay()
        })
    }
}
