//
//  ConsultClaimsViewController.swift
//  Afore
//
//  Created by antonio lavin on 12/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import DatePickerDialog
import AEXML
import SWXMLHash

class ConsultClaimsViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var backIconImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rightIconImageView: UIImageView!
    
    var dataColor: Int?
    var dataIconRight: String?
    var folioSinester:String?
    var dateBorn:String?
    
    @IBOutlet weak var dateBornTextField: UITextField!
    @IBOutlet weak var folioEditText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        backButton.setTitleColor(UIColor (netHex:dataColor!) , for: .normal)
        addRightIcon()
        backIconImageView.image = backIconImageView.image!.withRenderingMode(.alwaysTemplate)
        backIconImageView.tintColor = UIColor (netHex:dataColor!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addRightIcon(){
        rightIconImageView.image = UIImage(named: dataIconRight!)
    }
    
    @IBAction func datePickerButton(_ sender: Any) {
        DatePickerDialog().show(title: "Seleccionar Fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) -> Void in
        
            if(date == nil){
                
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let dateString = dateFormatter.string(from:date!)
                self.dateBornTextField.text = dateString
            }
        }
    }
    
    func convertFormatOfDate(date: String, originalFormat: String, destinationFormat: String) -> String! {
        
        // Orginal format :
        let dateOriginalFormat = DateFormatter()
        dateOriginalFormat.dateFormat = originalFormat      // in the example it'll take "yy MM dd" (from our call)
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.dateFormat = destinationFormat // in the example it'll take "EEEE dd MMMM yyyy" (from our call)
        
        // Convert current String Date to NSDate
        let dateFromString = dateOriginalFormat.date(from: date)
        
        // Convert new NSDate created above to String with the good format
        let dateFormated = dateDestinationFormat.string(from: dateFromString!)
        
        return dateFormated
        
    }
    
    @IBAction func consultButton(_ sender: Any) {
        dateBorn = dateBornTextField.text
        folioSinester = folioEditText.text
        
        if(folioSinester == ""){
            AlertDialog.show(title: "Aviso", body: "Es necesario ingresar el folio", view: self)
        }else{
            consultSOAPWs()
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func consultSOAPWs(){
        let soapMessage = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sin=\"http://siniestros.acsel.com.mx/\"><soapenv:Header/><soapenv:Body><sin:consultaSiniestro><foliosin>54561</foliosin><fecnaci>20/08/1946</fecnaci></sin:consultaSiniestro></soapenv:Body></soapenv:Envelope>"
        
        let urlStr = "http://200.38.108.80/SegurosServicios/SinBitServer/?wsdl"
        
        if let url = URL(string: urlStr) {
            
            let request = NSMutableURLRequest(url: url)
            request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue((soapMessage), forHTTPHeaderField: "Content-Length")
            request.httpMethod = "POST"
            request.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                //print("\(String(response!)))")
                if error == nil {
                    if let data = data {
                        let result = String(data: data, encoding: String.Encoding.utf8)
                        //print("\(String(result!))")
                        self.parsingXMLData(data: result!)
                    }
                }
            }).resume()
        }
    }
    
    func parsingXMLData(data: String){
        do {
            let xmlDoc = try AEXMLDocument(xml: data)
            //print(xmlDoc.xml)
        
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["errorCode"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["errorDesc"].string)
            
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["comentario"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["fecha"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["fechaComen"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["producto"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["status"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["historico"]["sucursal"].string)
            
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["comentario"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["fecha"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["fechaComen"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["producto"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["status"].string)
            print(xmlDoc.root["SOAP-ENV:Body"]["ns2:consultaSiniestroResponse"]["SinBitResponse"]["ultimoSiniestro"]["sucursal"].string)
            
        }
        catch {
            print("\(error)")
        }
    }
}

